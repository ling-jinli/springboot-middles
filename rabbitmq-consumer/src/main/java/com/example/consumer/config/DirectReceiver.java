package com.example.consumer.config;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: ziling
 * @date: created in 下午2:52 2022/7/10
 * @Description:
 */
@Component
// 监听队列名称
@RabbitListener(queues = "TestDirectQueue")
public class DirectReceiver {
    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("DirectReceiver 消费者收到消息："+ testMessage.toString());
    }

}