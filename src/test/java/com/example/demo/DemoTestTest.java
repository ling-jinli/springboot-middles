package com.example.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/13 16:08
 */
public class DemoTestTest {

    @Before
    public void setUp() throws Exception {
        System.out.println("before");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("shut down");
    }

    @Test
    public void say() {
        System.out.println("hi");
    }
}
