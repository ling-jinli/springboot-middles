package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.bean.Account;
import org.apache.ibatis.annotations.Mapper;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/31 15:11
 */
@Mapper
public interface AccountMapper extends BaseMapper<Account> {
}
