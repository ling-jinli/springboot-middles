package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.bean.TestUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 9:53
 */
@Mapper
public interface TestUserMapper extends BaseMapper<TestUser> {

    /**
     * 通过用户名匹配用户列表
     *
     * @param userName 用户名
     * @return list
     */
    List<TestUser> getByName(@Param("userName") String userName);
    /**
     * 通过id匹配用户列表
     *
     * @param id pk
     * @return pojo
     */
    TestUser findUserById(@Param("id") int id);
}
