package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.weather.entity.ChinaCitys;
import org.apache.ibatis.annotations.Mapper;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:15
 */
@Mapper
public interface ChinaCitysMapper extends BaseMapper<ChinaCitys> {
}
