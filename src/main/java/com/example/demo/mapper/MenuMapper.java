package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.bean.Menu;
import com.example.demo.vo.MenuVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/2/14 14:17
 * @desc [xxx]
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<MenuVO> lazyList(@Param("parentId") Long parentId, Map<String, Object> param);
}
