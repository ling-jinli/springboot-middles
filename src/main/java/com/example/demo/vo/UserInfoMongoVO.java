package com.example.demo.vo;

import com.example.demo.domain.UserInfoMongo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author: ziling
 * @date: created in 下午6:49 2022/7/10
 * @Description:
 */
@Data
public class UserInfoMongoVO extends UserInfoMongo {
    // 抄表方式
    private String writeMethod;
    // 初始化状态   0不可1可以
    private String meterReadingStatus;
    // 抄表例日
    private Integer shouldWriteDays;
    // 抄表周期
    private String meterReadingCycle;
    // 区段名称
    private String writeSectionName;
    // 抄表员
    private Long writorId;
    // 用户号
    private List<String> userNos;
    private List<String> deptNos;
    private List<String> writeSectionNos;//抄表区段
    // 电度电费 VOLUME_CHARGE decimal(14,4) 14 4 FALSE FALSE FALSE
    private BigDecimal volumeChargeSum;
    // 基本电费 BASIC_MONEY decimal(14,4) 14 4 FALSE FALSE FALSE
    private BigDecimal basicMoneySum;
    // 力调电费 POWER_RATE_MONEY decimal(14,4) 14 4 FALSE FALSE FALSE
    private BigDecimal powerRateMoneySum;
    // 总退补电费
    private BigDecimal refundMoneySum;
    // 附加费 SURCHARGES decimal(14,4) 14 4 FALSE FALSE FALSE
    private BigDecimal surchargesSum;
    // 总电费
    private BigDecimal totalAmountSum;
    // 有功总电量
    private BigDecimal totalPowerSum;
    // 无功总电量
    private BigDecimal qTotalPowerSum;
    //合计电量
    private BigDecimal powerSum;
    // 抄表序号
    private Integer writeSn;
    private List<Long> writorIds;
    // 变压器名称
    private String transformerName;
}
