package com.example.demo.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/11 14:31
 * [es配置中心]
 * 服务器搭建在centOs虚拟机上
 */
@Configuration
public class ElasticSearchRestClientConfig {

    @Bean
    public RestHighLevelClient restHighLevelClient() {
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("192.168.19.133", 9200, "http")
                ));
        return restHighLevelClient;
    }
}
