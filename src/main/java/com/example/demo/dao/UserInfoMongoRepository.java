package com.example.demo.dao;

import com.example.demo.bean.MongoTest;
import com.example.demo.domain.UserInfoMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: ziling
 * @date: created in 下午7:21 2022/7/10
 * @Description:
 */
public interface UserInfoMongoRepository extends MongoRepository<UserInfoMongo, String> {
}
