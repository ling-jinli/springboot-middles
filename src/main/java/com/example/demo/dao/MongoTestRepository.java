package com.example.demo.dao;


import com.example.demo.bean.MongoTest;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/17 16:32
 */
public interface MongoTestRepository extends MongoRepository<MongoTest, String> {
}
