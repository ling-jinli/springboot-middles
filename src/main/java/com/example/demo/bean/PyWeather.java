package com.example.demo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/5 15:35
 */
@Data
@NoArgsConstructor
@TableName("py_weather")
public class PyWeather implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.UUID)//指定自增策略
    private Integer id;
    private String accountDate;
    private String hmax;
    private String hmin;
    private String hgl;
    private String fe;
    private String wk;
    private String releaseTime;
}
