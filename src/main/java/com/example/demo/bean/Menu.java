package com.example.demo.bean;/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 实体类
 *
 * @author Chill
 */
@Data
@TableName("blade_menu")
@ApiModel(value = "Menu对象", description = "Menu对象")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 菜单父主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "菜单父主键")
    private Long parentId;

    /**
     * 菜单编号
     */
    @ApiModelProperty(value = "菜单编号")
    private String code;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;

    /**
     * 菜单别名
     */
    @ApiModelProperty(value = "菜单别名")
    private String alias;

    /**
     * 请求地址
     */
    @ApiModelProperty(value = "请求地址")
    private String path;

    /**
     * 菜单资源
     */
    @ApiModelProperty(value = "菜单资源")
    private String source;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 菜单类型
     */
    @ApiModelProperty(value = "菜单类型")
    private Integer category;

    /**
     * 操作按钮类型
     */
    @ApiModelProperty(value = "操作按钮类型")
    private Integer action;

    /**
     * 是否打开新页面
     */
    @ApiModelProperty(value = "是否打开新页面")
    private Integer isOpen;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 是否已删除
     */
    @TableLogic
    @ApiModelProperty(value = "是否已删除")
    private Integer isDeleted;

    /**
     * 菜单用途
     */
    @ApiModelProperty(value = "菜单用途")
    private Integer menuType;


    @ApiModelProperty(value = "激活时显示的图标")
    private String activeIcon;

    @ApiModelProperty(value = "非激活时显示的图标")
    private String inactiveIcon;

    @ApiModelProperty(value = "前端是否缓存(0.不缓存  1.缓存)")
    private Integer keepAlive;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        Menu other = (Menu) obj;
        return false;
    }

}
