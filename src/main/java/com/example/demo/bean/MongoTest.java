package com.example.demo.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/17 16:29
 * [mongodb测试对象]
 */
@Data
// 文档对象（表名）
@Document(collection = "user")
public class MongoTest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private String name;
    private Integer age;
    private String sex;
    private String add;
}
