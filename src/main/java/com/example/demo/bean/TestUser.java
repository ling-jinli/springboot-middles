package com.example.demo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 9:50
 */
@Data
@NoArgsConstructor
@TableName("test_user")
public class TestUser implements Serializable {
    private static final long serialVersionUID = -5644799954031156649L;
    @TableId(value = "id", type = IdType.AUTO)//指定自增策略
    private Integer id;
    private String userName;
    private String passWord;
    private Integer sex;
    private Date birthday;
}
