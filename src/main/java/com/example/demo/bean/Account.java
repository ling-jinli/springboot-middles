package com.example.demo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/31 15:09
 */
@Data
@NoArgsConstructor
@TableName("account")
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)//指定自增策略
    private Integer id;
    private String username;
    private String password;
    private String perms;
    private String role;
}
