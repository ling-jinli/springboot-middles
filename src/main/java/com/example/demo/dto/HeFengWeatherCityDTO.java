package com.example.demo.dto;

import com.example.demo.weather.entity.Location;
import com.example.demo.weather.entity.Refer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/19 9:18
 * <p>
 * [城市]
 */
@Data
public class HeFengWeatherCityDTO {
    @ApiModelProperty("响应码")
    private String code;

    @ApiModelProperty("地区")
    private List<Location> location;

    @ApiModelProperty("原始数据")
    private Refer refer;
}
