package com.example.demo.dto;

import com.example.demo.weather.entity.Location;
import com.example.demo.weather.entity.Now;
import com.example.demo.weather.entity.Refer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 实时天气
 *
 * @author lxl & 2022/1/18 10:58 AM
 */
@Data
public class HeFengRealTimeWeatherDTO {

    @ApiModelProperty("响应码")
    private String code;

    @ApiModelProperty("地区")
    private List<Location> location;

    @ApiModelProperty("原始数据")
    private Refer refer;

    @ApiModelProperty("当前API的最近更新时间")
    private String updateTime;

    @ApiModelProperty("当前数据的响应式页面，便于嵌入网站或应用")
    private String fxLink;

    @ApiModelProperty("实时天气数据")
    private Now now;

}
