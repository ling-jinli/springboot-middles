package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.example.demo.bean.MongoTest;
import com.example.demo.service.IMongoTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/17 16:40
 */
@Slf4j
@RestController
@RequestMapping("/mongodb")
public class MongoTestController {
    @Autowired
    private IMongoTestService mongoTestService;

    @PostMapping(value = "/test1")
    public void saveTest(@RequestBody String json) throws Exception {
        MongoTest mongoTest = new MongoTest();
        // 将json数据对象解析成对象
        if (ObjectUtils.isNotEmpty(json)) {
            mongoTest = JSONObject.parseObject(json, mongoTest.getClass());
        }
        mongoTestService.saveTest(mongoTest);
    }

    @GetMapping(value = "/test2")
    public List<MongoTest> findTestById() {
        List<MongoTest> mgtests = mongoTestService.getAll();
        log.info("mgtest信息： {}", mgtests.toString());
        return mgtests;
    }

    @PostMapping(value = "/test3")
    public void updateTest(@RequestBody String json) {
        MongoTest mongoTest = new MongoTest();
        // 将json数据对象解析成对象
        mongoTest = JSONObject.parseObject(json, mongoTest.getClass());
        mongoTestService.saveTest(mongoTest);
    }

    @PostMapping(value = "/test4")
    public void deleteTestById(String primaryKey) {
        mongoTestService.deleteTestById(primaryKey);
    }
}
