package com.example.demo.controller;

import com.example.demo.service.IWeatherService;
import com.example.demo.weather.vo.WeatherVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/19 9:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/app/v1/weather")
@Api(value = "天气控制器", tags = "天气控制器")
public class WeatherController {

    private final IWeatherService weatherService;

    /**
     * 传入坐标，获取实时天气
     */
    @GetMapping("/real-time")
    @ApiOperation(value = "传入坐标，获取实时天气")
    public WeatherVO detail(@RequestParam String location) {
        WeatherVO weatherVO = weatherService.realTimeWeather(location);
        return weatherVO;
    }

}
