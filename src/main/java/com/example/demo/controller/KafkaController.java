package com.example.demo.controller;

import com.example.demo.middle.kafka.KafkaProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 15:43
 * 发送消息
 */
@Slf4j
@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {
    private static final SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Autowired
    KafkaProducer kafkaProducer;

    @RequestMapping("send")
    @ResponseBody
    public String sendMessage(String msg) {
        try {
            log.warn("发送时间:{},发送消息:{}", sf.format(new Date()), msg);
            kafkaProducer.send(msg);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

}
