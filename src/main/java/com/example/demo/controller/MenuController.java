package com.example.demo.controller;

import com.example.demo.service.IMenuService;
import com.example.demo.vo.MenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * MenuController 菜单控制器
 *
 * @author lxl & 2021/4/1 10:42 上午
 */
@RestController
@AllArgsConstructor
@RequestMapping("/menu")
@Api(value = "菜单", tags = "菜单")
public class MenuController {

    private final IMenuService menuService;

    /**
     * 懒加载列表
     */
    @GetMapping("/lazy-list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "菜单编号", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "name", value = "菜单名称", paramType = "query", dataType = "string")
    })
    @ApiOperation(value = "懒加载列表", notes = "传入menu")
    public List<MenuVO> lazyList(Long parentId, @ApiIgnore @RequestParam Map<String, Object> menu) {
        List<MenuVO> list = menuService.lazyList(parentId, menu);
        return list;
    }
}
