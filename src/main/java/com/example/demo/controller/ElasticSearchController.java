package com.example.demo.controller;

import com.example.demo.bean.User;
import com.example.demo.service.ElasticSearchService;
import com.example.demo.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/14 14:19
 * 【es分词器服务入口】
 * indexName:test_index
 */
@RestController
@RequestMapping(value = "es/test")
@Slf4j
public class ElasticSearchController {
    @Autowired
    private ElasticSearchService elasticSearchService;

    @PostMapping("/createIndex")
    public String createIndexTest(String index) throws Exception {
        // 创建索引
        boolean indexCreateFlag = elasticSearchService.createIndex(index);
        log.info("创建{}标识：{}", index, indexCreateFlag ? "成功" : "异常");
        return indexCreateFlag ? "创建成功" : "创建异常";
    }

    @GetMapping("/existIndexTest")
    public String existIndexTest(String index) throws Exception {
        // 判断索引是否存在
        boolean flag = elasticSearchService.existIndex(index);
        log.info("标识：{}", index, flag ? "存在" : "不存在");
        return flag ? "存在" : "不存在";
    }

    // 类型+id
    @GetMapping("/getDocumentTest")
    String getDocumentTest(String index, String id) throws Exception {
        // 获取文档
        return elasticSearchService.getDocument(index, id);
    }

    // 类型+关键字 模糊查询
    @PostMapping("/searchRequestTest")
    List<Map<String, Object>> searchRequestTest(String index, String keyword) throws Exception {
        // 搜索请求
        return elasticSearchService.searchRequest(index, keyword);
    }

    /**
     * todo 下面是增删改
     */
    @PostMapping("/searchAllRequestTest")
    List<Integer> searchAllRequestTest() throws Exception {
        // 搜索请求
        return elasticSearchService.searchAllRequest("test_index");
    }

    @PostMapping("/updateDocumentTest")
    boolean updateDocumentTest(String index, User user) throws Exception {
        return elasticSearchService.updateDocument(index, user.getId().toString(), JsonUtils.objectToJson(user));
    }

    @PostMapping("/deleteDocumentTest")
    boolean deleteDocumentTest(String index, String id) throws Exception {
        // 删除文档
        return elasticSearchService.deleteDocument(index, id);
    }

    @PostMapping("/bulkRequestTest")
    void bulkRequestTest() throws Exception {
        // 批量插入
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setId((long) i);
            user.setAge(i);
            user.setName("测试name" + i);
            user.setDescription("测试des" + i);
            users.add(user);
        }
        elasticSearchService.bulkRequest("test_index", users);
    }

    @PostMapping("/creatRecord")
    void creatRecord(String index, User user) throws Exception {
        List<User> users = new ArrayList<>();
        users.add(user);
        elasticSearchService.bulkRequest(index, users);
    }


}
