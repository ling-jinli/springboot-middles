package com.example.demo.controller;


import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.log.StaticLog;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping(value = "/hutool")
public class CaptchaController {

    @GetMapping("/getCaptcha")
    @ApiOperation(value = "获取验证码")
    public void getCaptcha(HttpServletResponse response) {
        //生成验证码图片
        LineCaptcha circleCaptcha = CaptchaUtil.createLineCaptcha(200/*宽*/, 100/*高*/, 4/*验证码显示几个*/, 50/*有几个线段*/);
        //告诉浏览器输出内容为jpeg类型的图片
        response.setContentType("image/jpeg");
        //禁止浏览器缓存
        response.setHeader("Pragma", "No-cache");
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            circleCaptcha.write(outputStream);
            String code = circleCaptcha.getCode();
            Console.log(code);
            //关闭流
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        String unicodeStr = "沉默王二";
//        String unicode = Convert.strToUnicode(unicodeStr);
//        System.out.println(unicode);
//        String dateStr = "2020-09-29";
//        Date date = DateUtil.parse(dateStr);
//        System.out.println(date);
//        StaticLog.info("张三是谁？{}",unicode);
//        Console.log(dateStr);
//        AES aes = SecureUtil.aes();
//        // 加密
//        String encry = aes.encryptHex("沉默王二");
//        System.out.println(encry);
//        // 解密
//        String oo = aes.decryptStr(encry);


//    }

}