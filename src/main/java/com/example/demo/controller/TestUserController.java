package com.example.demo.controller;

import com.example.demo.bean.TestUser;
import com.example.demo.service.ITestUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 10:05
 */
@Slf4j
@RestController
@RequestMapping("/test/user")
public class TestUserController {

    @Autowired
    private ITestUserService userService;

    @RequestMapping("/queryAll")
    public List<TestUser> queryAll() {
        return userService.queryAll();
    }

    @GetMapping("/queryByName")
    public List<TestUser> queryByName(String userName) {
        return userService.queryByName(userName);
    }

    @GetMapping("/findUserById")
    public TestUser findUserById(@RequestParam int id) {
        return userService.findUserById(id);
    }


    @RequestMapping("/model/index")
    public String index(ModelMap modelMap) {
        System.out.println(modelMap.get("words"));
        return (String) modelMap.get("words");
    }

    /**
     * 也可以通过@ModelAttribute获取
     */
    @RequestMapping("/attribute/index")
    public String index(@ModelAttribute("words") String words) {
        System.out.println(words);
        return words;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public void users() {
        throw new RuntimeException("没有任何用户。");
    }

    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    public void test1(Date date) {
        date = new Date();
        // Mon Jan 17 14:29:35 CST 2022
        System.out.println(date);
    }

}
