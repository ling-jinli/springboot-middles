package com.example.demo.controller;

import com.example.demo.bean.TestUser;
import com.example.demo.domain.UserInfoMongo;
import com.example.demo.service.ITestUserService;
import com.example.demo.service.IUserInfoMongoService;
import com.example.demo.vo.UserInfoMongoVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: ziling
 * @date: created in 下午7:04 2022/7/10
 * @Description:
 */
@Slf4j
@RestController
@RequestMapping("/mongo/user")
public class UserInfoMongoController {

    @Autowired
   private IUserInfoMongoService userInfoMongoService;

    @GetMapping("/queryByCondition")
    public Page<UserInfoMongo> queryByName(UserInfoMongoVO userInfoMongoVO) {
        return userInfoMongoService.queryByCondition(userInfoMongoVO);
    }

}
