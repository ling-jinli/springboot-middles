package com.example.demo.controller;

import com.example.demo.bean.Account;
import com.example.demo.bean.TestUser;
import com.example.demo.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/31 15:27
 */
@RestController
@RequestMapping(value = "/account")
public class AccountController {
    @Autowired
    IAccountService accountService;

    @RequestMapping("/queryAll")
    public List<Account> queryAll() {
        return accountService.list();
    }
}
