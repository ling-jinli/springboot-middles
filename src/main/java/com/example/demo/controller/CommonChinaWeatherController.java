package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.constant.ParameterConstant;
import com.example.demo.service.IChinaCitysService;
import com.example.demo.util.WeatherOpenApi;
import com.example.demo.weather.entity.ChinaCitys;
import com.example.demo.weather.entity.Current;
import com.example.demo.weather.vo.QueryCommonVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:20
 * [中国-天气-通用服务接口]
 */
@Slf4j
@RestController
@RequestMapping(value = "/common/weather")
public class CommonChinaWeatherController {
    private static final String DISTRICT_COLUMN = "district";
    private static final String CITY_COLUMN = "city";
    @Autowired
    IChinaCitysService cityService;

    @RequestMapping("/queryByName")
    public Current queryByName(QueryCommonVO queryCommonVO) {
        // 第一步 判断该城市是否存在与系统中
        String cityName = queryCommonVO.getCityName();
        String districtName = queryCommonVO.getDistrictName();
        ChinaCitys cityInfo = cityService.getOne(new QueryWrapper<ChinaCitys>().eq(CITY_COLUMN, cityName).eq(DISTRICT_COLUMN, districtName));
        // cityId
        String cityId = cityInfo.getCityId();
        String response = WeatherOpenApi.responseData(ParameterConstant.WEATHER_CURR_AND_15D_24H_URL, cityId);
        // 截取字符
        int beginIndex = response.indexOf("\"current\":{\"airpressure\":");
        int endIndex = response.indexOf("},\"nongLi\":");
        String currentInfo = response.substring(beginIndex, endIndex) + "}";
        String objectJson = currentInfo.replace("\"current\":", "");
        // 将json转对象VO
        Current current = JSONObject.parseObject(objectJson, Current.class);
        return current;
    }

}
