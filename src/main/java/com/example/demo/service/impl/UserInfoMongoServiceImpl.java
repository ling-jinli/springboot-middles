package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.api.R;
import com.example.demo.dao.MongoTestRepository;
import com.example.demo.dao.UserInfoMongoRepository;
import com.example.demo.domain.UserInfoMongo;
import com.example.demo.service.IUserInfoMongoService;
import com.example.demo.util.JsonUtils;
import com.example.demo.vo.UserInfoMongoVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author: ziling
 * @date: created in 下午6:54 2022/7/10
 * @Description:
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserInfoMongoServiceImpl implements IUserInfoMongoService {

    private final UserInfoMongoRepository userInfoMongoRepository;

    @Override
    public Page<UserInfoMongo> queryByCondition(UserInfoMongoVO userInfoMongoVO) {

        /**
         * 排序规则
         */
        Sort sort = Sort.by("createDate").descending();

        /**
         * 分页
         */
        PageRequest pageRequest = PageRequest.of(0, 10, sort);

        /**
         * 分页查询
         */
        Page<UserInfoMongo> articleRepoAll = userInfoMongoRepository.findAll(pageRequest);

        /**
         * 打印
         */
        log.info(JsonUtils.objectToJson(articleRepoAll.getContent()));

        return articleRepoAll;
    }
}
