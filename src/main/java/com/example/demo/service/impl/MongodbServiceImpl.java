package com.example.demo.service.impl;

import com.example.demo.bean.MongoTest;
import com.example.demo.dao.MongoTestRepository;
import com.example.demo.service.IMongoTestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/17 17:41
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MongodbServiceImpl implements IMongoTestService {

    private final MongoTestRepository mongoTestRepository;

    @Override
    public void saveTest(MongoTest test) {
        mongoTestRepository.save(test);
    }

    @Override
    public void deleteTestById(String id) {
        mongoTestRepository.deleteById(id);
    }

    @Override
    public List<MongoTest> getAll() {
        return mongoTestRepository.findAll();
    }
}
