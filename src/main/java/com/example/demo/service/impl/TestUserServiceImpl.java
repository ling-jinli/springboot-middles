package com.example.demo.service.impl;

import com.example.demo.bean.TestUser;
import com.example.demo.config.RedisConfig;
import com.example.demo.mapper.TestUserMapper;
import com.example.demo.service.ITestUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 10:01
 * 自定义匹配mapper的xml
 */
@Slf4j
@Service
public class TestUserServiceImpl implements ITestUserService {

    private static final String REDIS_USER_KEY = "test-user-";
    @Resource
    TestUserMapper testUserMapper;
    @Resource
    RedisConfig redisConfig;

    /**
     * 查所有用户
     */
    @Override
    public List<TestUser> queryAll() {
        return testUserMapper.selectList(null);
    }

    @Override
    public List<TestUser> queryByName(String userName) {
        return testUserMapper.getByName(userName);
    }

    /**
     * 查id用户+redis
     */
    @Override
    public TestUser findUserById(int id) {
        String key = REDIS_USER_KEY + id;
        boolean hasKey = redisConfig.hasKey(key);
        if (hasKey) {
            TestUser TestUser = (TestUser) redisConfig.get(key);
            log.info("从【缓存中】获取数据:{}", TestUser.toString());
            return TestUser;
        } else {
            TestUser TestUser = testUserMapper.findUserById(id);
            log.info("从【数据库中】获取数据:{}", TestUser.toString());
            System.out.println("------------写入缓存---------------------");
            //写入缓存
            redisConfig.set(key, TestUser, 3600);
            return TestUser;
        }
    }
}
