package com.example.demo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.dto.HeFengRealTimeWeatherDTO;
import com.example.demo.dto.HeFengWeatherCityDTO;
import com.example.demo.service.IWeatherService;
import com.example.demo.util.HttpRequestUtil;
import com.example.demo.weather.entity.Location;
import com.example.demo.weather.entity.Now;
import com.example.demo.weather.vo.WeatherVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/19 9:15
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherServiceImpl implements IWeatherService {

    private static final String WEATHER_SERVICE_KEY = "bcd2ece27df24548a3e58e4517bea542";
    private static final String WEATHER_PROD_BASE_URL = "https://geoapi.qweather.com";
    private static final String GET_CITY_URL = "/v2/city/lookup";
    private static final String GET_REAL_TIME_WEATHER_URL = "/v7/weather/now";

    @Override
    public WeatherVO realTimeWeather(String location) {
        WeatherVO weatherVO = new WeatherVO();
        String parameter = "key=" + WEATHER_SERVICE_KEY + "&location=" + location;
        // 请求 url 拼接
        String response = HttpRequestUtil.sendGet(WEATHER_PROD_BASE_URL + GET_CITY_URL, parameter);
        // 将json转对象VO
        HeFengWeatherCityDTO heFengWeatherCityDTO = JSONObject.parseObject(response, HeFengWeatherCityDTO.class);
        if (heFengWeatherCityDTO != null) {
            String code = heFengWeatherCityDTO.getCode();
            if (code != null && code.equalsIgnoreCase("200")) {
                List<Location> city = heFengWeatherCityDTO.getLocation();
                if (city != null) {
                    Location oneCity = city.get(0);
                    if (oneCity != null) {
                        String cityName = oneCity.getName();
                        String cityId = oneCity.getId();
                        weatherVO.setCityName(cityName);
                        if (cityId != null) {
                            // 请求 url 拼接
                            String parameter1 = "key=" + WEATHER_SERVICE_KEY + "&location=" + cityId;
                            String responseObj = HttpRequestUtil.sendGet(WEATHER_PROD_BASE_URL + GET_REAL_TIME_WEATHER_URL, parameter1);
                            // 将json转对象VO
                            HeFengRealTimeWeatherDTO heFengRealTimeWeatherDTO = JSONObject.parseObject(responseObj, HeFengRealTimeWeatherDTO.class);
                            if (heFengRealTimeWeatherDTO != null) {
                                String code1 = heFengRealTimeWeatherDTO.getCode();
                                if (code1 != null && code1.equalsIgnoreCase("200")) {
                                    Now nowWeather = heFengRealTimeWeatherDTO.getNow();
                                    if (nowWeather != null) {
                                        String obsTime = nowWeather.getObsTime();
                                        String temp = nowWeather.getTemp();
                                        String text = nowWeather.getText();
                                        weatherVO.setObsTime(obsTime);
                                        weatherVO.setTemperature(temp);
                                        weatherVO.setWeatherDescribe(text);
                                    }
                                }
                            }

                        }

                    }
                }

            }
        }
        return weatherVO;
    }
}
