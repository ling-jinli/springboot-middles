package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.bean.Account;
import com.example.demo.mapper.AccountMapper;
import com.example.demo.service.IAccountService;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/31 15:15
 * todo 匹配通用型<泛型> 【建议使用】
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {
}
