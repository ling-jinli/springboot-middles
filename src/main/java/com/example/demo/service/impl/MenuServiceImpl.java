package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.bean.Menu;
import com.example.demo.mapper.MenuMapper;
import com.example.demo.service.IMenuService;
import com.example.demo.util.ForestNodeMerger;
import com.example.demo.vo.MenuVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/2/14 14:13
 * @desc [xxx]
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {
    @Override
    public List<MenuVO> lazyList(Long parentId, Map<String, Object> param) {
        if (param.get("parentId") == null) {
            parentId = null;
        }
        List<MenuVO> list = baseMapper.lazyList(parentId, param);
        // 将这里的目录结构树形化
        return ForestNodeMerger.merge(list);
    }
}
