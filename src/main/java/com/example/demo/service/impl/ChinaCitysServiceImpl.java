package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.ChinaCitysMapper;
import com.example.demo.service.IChinaCitysService;
import com.example.demo.weather.entity.ChinaCitys;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:15
 */
@Service
public class ChinaCitysServiceImpl extends ServiceImpl<ChinaCitysMapper, ChinaCitys> implements IChinaCitysService {
}
