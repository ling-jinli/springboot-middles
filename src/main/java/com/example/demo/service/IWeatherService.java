package com.example.demo.service;

import com.example.demo.weather.vo.WeatherVO;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/19 9:15
 */
public interface IWeatherService {
    /**
     * 传入坐标，获取实时天气
     *
     * @param location 坐标 格式：116.41,39.92
     * @return WeatherVO
     */
    WeatherVO realTimeWeather(String location);
}
