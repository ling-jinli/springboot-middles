package com.example.demo.service;

import com.example.demo.bean.MongoTest;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/17 17:39
 */
public interface IMongoTestService {

    void saveTest(MongoTest test);

    void deleteTestById(String id);

    List<MongoTest> getAll();

}
