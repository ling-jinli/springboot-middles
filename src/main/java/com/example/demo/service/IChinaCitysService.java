package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.weather.entity.ChinaCitys;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:14
 */
public interface IChinaCitysService extends IService<ChinaCitys> {
}
