package com.example.demo.service;

import com.example.demo.bean.TestUser;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/31 15:01
 */
public interface ITestUserService{

    List<TestUser> queryAll();

    List<TestUser> queryByName(String userName);

    TestUser findUserById(int id);
}
