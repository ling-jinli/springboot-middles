package com.example.demo.service;

import com.example.demo.domain.UserInfoMongo;
import com.example.demo.vo.UserInfoMongoVO;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author: ziling
 * @date: created in 下午6:53 2022/7/10
 * @Description:
 */
public interface IUserInfoMongoService {

    Page<UserInfoMongo> queryByCondition(UserInfoMongoVO userInfoMongoVO);
}
