package com.example.demo.service;

import com.example.demo.vo.MenuVO;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/2/14 14:14
 * @desc [xxx]
 */
public interface IMenuService {
    /**
     * 懒加载列表
     *
     * @param parentId
     * @param param
     * @return
     */
    List<MenuVO> lazyList(Long parentId, Map<String, Object> param);
}
