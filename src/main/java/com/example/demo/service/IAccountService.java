package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.bean.Account;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/31 15:13
 */
public interface IAccountService extends IService<Account> {
}
