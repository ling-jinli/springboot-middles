package com.example.demo.util;

import java.io.Serializable;
import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/2/14 11:53
 * @desc 节点
 */
public interface INode<T> extends Serializable {
    Long getId();

    Long getParentId();

    List<T> getChildren();

    default Boolean getHasChildren() {
        return false;
    }

}
