package com.example.demo.util;

import com.jcraft.jsch.*;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Properties;

/**
 * sftp工具类
 * */
public class SFTPutil {

	static {
		try {
			System.out.println("hello world");
		} catch (Exception e) {
			throw new RuntimeException("配置文件读取失败！");
		}
	}

	/**
	 * 登录
	 */
		public static ChannelSftp connectFTP() {
			ChannelSftp nChannelSftp = null;
			Session session = null;
			try {
				JSch jSch = new JSch();
				session = jSch.getSession("SXDNSZ_fan","180.169.95.129",22);
				// 设置超时时间
                // session.setTimeout(3600*1000);
				session.setPassword("d4JMY!yC");
				// 5.实例化Properties
				Properties nSSHConfig = new Properties();
				// 6.设置配置信息
				nSSHConfig.put("StrictHostKeyChecking", "no");
				// 7.session中设置配置信息
				session.setConfig(nSSHConfig);
				// 8.session连接
				session.connect();
				// 9.打开sftp通道
				Channel channel = session.openChannel("sftp");
				// 10.开始连接
				channel.connect();
				nChannelSftp = (ChannelSftp) channel;
				System.out.println("连接到主机"  + nChannelSftp.getHome());
			}catch (Exception e){
				e.printStackTrace();
			}
			return nChannelSftp;
		}

		/**
		 * 创建目录
		 */
		public static void createDir(ChannelSftp sftp, String createPath){
			try {
				if(isDirExist(sftp,createPath)){

					return;
				}
				String pathArry[] = createPath.split("/");
				StringBuffer filePath = new StringBuffer("/");
				for(String path : pathArry){
					if(path.equals("")){
						continue;
					}
					filePath.append(path+"/");
				}
				if(!isDirExist(sftp,filePath.toString())){
					sftp.mkdir(filePath.toString());
				}
			}catch (Exception e){
				e.printStackTrace();
			}

		}

		/**
		 * 判断目录是否存在
		 * @param sftp
		 * @param dirExist
		 * @return
		 */
		public static Boolean isDirExist(ChannelSftp sftp, String dirExist){
			try {
				SftpATTRS sftpATTRS = sftp.lstat(dirExist);
				return sftpATTRS.isDir();
			}catch (Exception e){
				e.printStackTrace();
				return false;
			}
		}


	public static void main(String[] args){
		try {
			connectFTP();
		}catch (Exception e){
			e.printStackTrace();
		}

	}

}
