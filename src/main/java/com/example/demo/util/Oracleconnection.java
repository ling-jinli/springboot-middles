package com.example.demo.util;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/5 10:24
 * oracle连接测试
 */
@Slf4j
public class Oracleconnection {

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:ORCL";
        String username = "test";
        String password = "test";
        String driver = "oracle.jdbc.driver.OracleDriver";
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
            log.info("连接成功...");
            return con;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

//    public static void main(String[] args) throws SQLException {
//        try {
//            Connection connection = getConnection();
//            if (!connection.equals(null)) {
//                Statement state = connection.createStatement();
//                String sql = "select * from py_weather";
//                //将sql语句上传至数据库执行
//                ResultSet resultSet = state.executeQuery(sql);
//                while (resultSet.next()) {
//                    System.out.println(resultSet.getString(1) + "--" + resultSet.getString(2));
//                }
//            }
//            // 关闭连接，防止内存溢出
//            connection.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

}
