package com.example.demo.util;

public enum GenerateDigitEnum {
    CUSTOMER_NO_GENERATE_DIGIT("CUSTOMER_NO", "客户生成位", "1000000000"),
    USER_NO_GENERATE_DIGIT("USER_NO", "用户生成位", "2000000000"),
    METER_NO_GENERATE_DIGIT("METERINFO_NO", "计量点生成位", "20000000000"),
    SETTLEMENT_NO_GENERATE_DIGIT("SETTLEMENT_NO", "结算户生成位", "3000000000"),
    RECEIVE_POWER_POINT_NO_DIGIT("RECEIVE_POWER_POINT_NO", "受电点编号生成", "7000000000"),
    POWER_NO_DIGIT("POWER_NO", "电源编号生成", "8000000000"),
    TG_NO_DIGIT("TG_NO", "台区", "900000000"),
    TRANSFORMER_NO_DIGIT("TRANSFORMER_NO", "变压器号", "500000000");

    /**
     * 生成对应code
     */
    private String code;
    /**
     * 生成对应名称
     */
    private String name;
    /**
     * 生成对应值
     */
    private String value;


    GenerateDigitEnum(String code, String name, String value) {
        this.code = code;
        this.name = name;
        this.value = value;

    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }


    //根据输入的资产类型的value值循环找出对应的枚举类型
    public static GenerateDigitEnum getEnumByCode(String code) {
        switch (code) {
            case "CUSTOMER_NO":
                return GenerateDigitEnum.CUSTOMER_NO_GENERATE_DIGIT;
            case "USER_NO":
                return GenerateDigitEnum.USER_NO_GENERATE_DIGIT;
            case "METERINFO_NO":
                return GenerateDigitEnum.METER_NO_GENERATE_DIGIT;
            case "SETTLEMENT_NO":
                return GenerateDigitEnum.SETTLEMENT_NO_GENERATE_DIGIT;
            case "RECEIVE_POWER_POINT_NO":
                return GenerateDigitEnum.RECEIVE_POWER_POINT_NO_DIGIT;
            case "POWER_NO":
                return GenerateDigitEnum.POWER_NO_DIGIT;
            case "TRANSFORMER_NO":
                return GenerateDigitEnum.TRANSFORMER_NO_DIGIT;
            default:
                return null;
        }
    }
}
