package com.example.demo.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/14 14:14
 */
@Slf4j
public class JsonUtils {
    private static ObjectMapper objectMapper = null;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 对象转换成json
     *
     * @param obj
     * @param <T>
     * @return
     */
    public static <T> String objectToJson(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj : objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.error("Parse Object to Json error", e);
            return null;
        }
    }

    /**
     * 将json转换成对象Class
     *
     * @param src
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T jsonToObject(String src, Class<T> clazz) {
        if (StringUtils.isEmpty(src) || clazz == null) {
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) src : objectMapper.readValue(src, clazz);
        } catch (Exception e) {
            log.warn("Parse Json to Object error", e);
            return null;
        }
    }

    /**
     * 字符串转换为 Map<String, Object>
     *
     * @param src
     * @return
     * @throws Exception
     */
    public static <T> Map<String, Object> jsonToMap(String src) {
        if (StringUtils.isEmpty(src)) {
            return null;
        }
        try {
            return objectMapper.readValue(src, Map.class);
        } catch (Exception e) {
            log.warn("Parse Json to Map error", e);
            return null;
        }

    }

    public static <T> List<T> jsonToList(String jsonArrayStr, Class<T> clazz) {
        try {
            JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, clazz);
            return (List<T>) objectMapper.readValue(jsonArrayStr, javaType);
        } catch (Exception e) {
            log.warn("Parse Json to Map error", e);
            return null;
        }

    }


//    public static void main(String[] args) {
////        List<String> aaa = new ArrayList<>();
////        aaa.add("aa");
////        aaa.add("bb");
////        aaa.add("cc");
////        aaa.add("cc");
////        aaa.add("aa");
////        ListSetMapUtil listSetMapUtil = new ListSetMapUtil();
////        String concat = listSetMapUtil.listContactToStr(aaa);
////        if (!concat.equals(null)){
////            System.out.println(concat);
////        }
////        List<String> listMultiElements = listSetMapUtil.getListMultiElements(aaa);
////        listMultiElements.forEach(s -> System.out.println(s));
//        List<String> names = new ArrayList<>();
//        names.add("张三");
//        names.add("张三1");
//        names.add("张三2");
//        names.add("张三3");
//        JSONObject object = new JSONObject();
//        object.put("name", "T_AR_UserInfo");
//        object.put("namse", names);
//        object.put("isMulti", "否");
//        object.put("tableComment", "用电户信息");
//
//        JSONObject object1 = new JSONObject();
//        object1.put("name", "T_AR_UserInfo1");
//        object1.put("isMulti", "否1");
//        object1.put("tableComment", "用电户信息1");
//        JSONArray jsonArray = new JSONArray();
//        jsonArray.add(object);
//        jsonArray.add(object1);
//        System.out.println(jsonArray.toJSONString());
//        CollectionUtils.isEmpty(new ArrayList<>());
//    }
}
