package com.example.demo.util;

import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/2/14 11:51
 * @desc 树形节点数据结构
 */
public class ForestNodeMerger {
    public ForestNodeMerger() {
    }

    public static <T extends INode<T>> List<T> merge(List<T> items) {
        ForestNodeManager<T> forestNodeManager = new ForestNodeManager(items);
        items.forEach((forestNode) -> {
            if (forestNode.getParentId() != 0L) {
                INode<T> node = forestNodeManager.getTreeNodeAt(forestNode.getParentId());
                if (node != null) {
                    node.getChildren().add(forestNode);
                } else {
                    forestNodeManager.addParentId(forestNode.getId());
                }
            }

        });
        return forestNodeManager.getRoot();
    }
}
