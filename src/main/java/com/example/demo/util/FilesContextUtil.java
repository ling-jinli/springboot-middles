package com.example.demo.util;

import java.io.*;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/10 16:18
 * 文件工具
 */
public class FilesContextUtil {

    /**
     * 读取文件 json
     * "D:\\weixin_download\\WeChat Files\\wxid_q5t0u23v05b922\\FileStorage\\File\\2022-01\\武汉.json";
     */
    public static String getFileStr(String filePath) {
        String jsonStr = "";
        try {
            File jsonFile = new File(filePath);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            System.out.println(jsonStr.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonStr;
    }


}
