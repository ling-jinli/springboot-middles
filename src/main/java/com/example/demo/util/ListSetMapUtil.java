package com.example.demo.util;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/4/18 14:01
 * @desc [list，set,map工具类]
 */
@Slf4j
public class ListSetMapUtil<T> {

    /**
     * 筛选重复记录
     */
    public List<T> getListMultiElements(List<T> words) {
        List<T> results = HashMultiset.create(words).entrySet().stream()
                .filter(w -> w.getCount() > 1)
                .map(Multiset.Entry::getElement)
                .collect(Collectors.toList());
        return results;
    }

    /**
     * 1:将list转换成逗号分隔的字符串
     * */
    public String listContactToStr(List<T> words){
        return words.stream().map(String::valueOf).collect(Collectors.joining(","));
    }


}
