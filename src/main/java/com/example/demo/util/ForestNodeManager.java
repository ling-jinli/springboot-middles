package com.example.demo.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/2/14 11:54
 * @desc 树形节点管理接口
 */
public class ForestNodeManager<T extends INode<T>> {
    private final ImmutableMap<Long, T> nodeMap;
    private final Map<Long, Object> parentIdMap = Maps.newHashMap();

    public ForestNodeManager(List<T> nodes) {
        this.nodeMap = Maps.uniqueIndex(nodes, INode::getId);
    }

    public INode<T> getTreeNodeAt(Long id) {
        return this.nodeMap.containsKey(id) ? (INode) this.nodeMap.get(id) : null;
    }

    public void addParentId(Long parentId) {
        this.parentIdMap.put(parentId, "");
    }

    public List<T> getRoot() {
        List<T> roots = new ArrayList();
        this.nodeMap.forEach((key, node) -> {
            if (node.getParentId() == 0L || this.parentIdMap.containsKey(node.getId())) {
                roots.add(node);
            }

        });
        return roots;
    }
}
