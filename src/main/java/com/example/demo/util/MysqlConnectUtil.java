package com.example.demo.util;

import java.sql.*;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 14:13
 */
public class MysqlConnectUtil {

    private static final String filePath = "D:\\项目相关\\中国城市-通用.txt";

    static Connection commonConnection() throws ClassNotFoundException, SQLException {
        //1.加载驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/bladexs?useSSL=false&useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&transformedBitIsBoolean=true&tinyInt1isBit=false&allowMultiQueries=true&serverTimezone=GMT%2B8&allowPublicKeyRetrieval=true";
        String user = "root";
        String password = "root";
        //2.建立连接
        Connection connections = DriverManager.getConnection(url, user, password);
        //返回连接对象
        return connections;
    }

    //调用它的方法如下：
    public static void query(String query) throws SQLException, ClassNotFoundException {
        //获取connection对象
        Connection connection = commonConnection();
        //3.准备SQL语句
        PreparedStatement pStatement = connection.prepareStatement(query);
        //4.执行SQL语句
        ResultSet resultSet = pStatement.executeQuery();
        //检索此 ResultSet对象的列的数量，类型和属性。
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        //返回此 ResultSet对象中的列数。
        int column = resultSetMetaData.getColumnCount();
        System.out.println("序号" + "\t" + "姓名" + "\t" + "年龄" + "\t" + "课程");
        //5.处理结果遍历要查询的数据
        while (resultSet.next()) {
            //遍历行数
            for (int i = 1; i <= column; i++) {
                System.out.print(resultSet.getObject(i) + "\t");
            }
            System.out.println();
        }
        //6.关闭连接
        resultSet.close();
        pStatement.close();
        connection.close();
    }


//    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
//        //获取connection对象
//        Connection con = commonConnection();
//        // 读取
//        FileReader fr = new FileReader(filePath);
//        BufferedReader reader = new BufferedReader(fr);
//        if (!reader.ready()) {
//            System.out.println("文件流暂时无法读取");
//            return;
//        }
//        String line;
//        PreparedStatement stmt = null;
//        while ((line = reader.readLine()) != null) {
//            System.out.print(line + "\n");
//            //101010100,北京,北京,北京
//            String[] split = line.split(",");
//            String sql = "insert into china_citys(city_id,district,city,province)values(?,?,?,?)";
//            stmt = con.prepareStatement(sql);
//            stmt.setString(1, split[0]);
//            stmt.setString(2, split[1]);
//            stmt.setString(3, split[2]);
//            stmt.setString(4, split[3]);
//            // 返回值代表收到影响的行数
//            int result = stmt.executeUpdate();
//            System.out.println("执行sql=>" + sql + ",结果标识：" + result);
//        }
//        reader.close();
//        stmt.close();
//        con.close();
//    }


}
