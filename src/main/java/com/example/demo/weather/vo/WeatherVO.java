package com.example.demo.weather.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/19 9:11
 */
@Data
public class WeatherVO implements Serializable {
    @ApiModelProperty("城市名称")
    private String cityName;

    @ApiModelProperty("当前温度")
    private String temperature;

    @ApiModelProperty("数据观测时间")
    private String obsTime;

    @ApiModelProperty("天气状况的文字描述，包括阴晴雨雪等天气状态的描述")
    private String weatherDescribe;

}

