package com.example.demo.weather.vo;

import com.example.demo.weather.entity.Forecast15d;
import com.example.demo.weather.entity.Forecast24h;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:09
 */
@Data
public class WeatherData implements Serializable {
    private static final long serialVersionUID = 1L;
    private CurrentVO current;
    private List<Forecast15d> forecast15d;
    private List<Forecast24h> forecast24h;
    private String message;
    private Object pageNum;
}
