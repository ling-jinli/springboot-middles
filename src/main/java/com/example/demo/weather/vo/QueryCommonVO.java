package com.example.demo.weather.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:31
 */
@Data
public class QueryCommonVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cityName;
    private String districtName;
}
