package com.example.demo.weather.vo;

import com.example.demo.weather.entity.Air;
import com.example.demo.weather.entity.Alerts;
import com.example.demo.weather.entity.Current;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:06
 */
@Data
public class CurrentVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Air air;
    private List<Alerts> alerts;
    private Current current;
    private String nongLi;
    private String tips;
}
