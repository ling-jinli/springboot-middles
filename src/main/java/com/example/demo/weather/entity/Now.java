package com.example.demo.weather.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 实时天气
 *
 * @author lxl & 2022/1/18 11:04 AM
 */
@Data
public class Now {

    @ApiModelProperty("数据观测时间")
    private String obsTime;

    @ApiModelProperty("温度，默认单位：摄氏度")
    private String temp;

    @ApiModelProperty("体感温度，默认单位：摄氏度")
    private String feelsLike;

    @ApiModelProperty("天气状况和图标的代码，图标可通过天气状况和图标下载")
    private String icon;

    @ApiModelProperty("天气状况的文字描述，包括阴晴雨雪等天气状态的描述")
    private String text;

    @ApiModelProperty("风向360角度")
    private String wind360;

    @ApiModelProperty("风向")
    private String windDir;

    @ApiModelProperty("风力等级")
    private String windScale;

    @ApiModelProperty("风速，公里/小时")
    private String windSpeed;

    @ApiModelProperty("相对湿度，百分比数值")
    private String humidity;

    @ApiModelProperty("当前小时累计降水量，默认单位：毫米")
    private String precip;

    @ApiModelProperty("大气压强，默认单位：百帕")
    private String pressure;

    @ApiModelProperty("能见度，默认单位：公里")
    private String vis;

    @ApiModelProperty("云量，百分比数值。可能为空")
    private String cloud;

    @ApiModelProperty("露点温度。可能为空")
    private String dew;
}

