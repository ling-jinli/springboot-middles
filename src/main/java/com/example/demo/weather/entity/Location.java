package com.example.demo.weather.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/19 9:18
 * [地区]
 */
@Data
public class Location {

    @ApiModelProperty("城市名称")
    private String name;

    @ApiModelProperty("城市ID")
    private String id;

    @ApiModelProperty("城市纬度")
    private String lat;

    @ApiModelProperty("城市经度")
    private String lon;

    @ApiModelProperty("城市的上级行政区划名称")
    private String adm2;

    @ApiModelProperty("城市所属一级行政区域")
    private String adm1;

    @ApiModelProperty("城市所属国家名称")
    private String country;

    @ApiModelProperty("城市所在时区")
    private String tz;

    @ApiModelProperty("城市目前与UTC时间偏移的小时数")
    private String utcOffset;

    @ApiModelProperty("城市是否当前处于夏令时 1.表示当前处于夏令时 0.表示当前不是夏令时")
    private String isDst;

    @ApiModelProperty("城市的属性")
    private String type;

    @ApiModelProperty("地区评分")
    private String rank;

    @ApiModelProperty("该地区的天气预报网页链接，便于嵌入你的网站或应用")
    private String fxLink;
}
