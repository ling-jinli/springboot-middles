package com.example.demo.weather.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:04
 * <p>
 * forecasttime: "15:00"
 * temperature: "11℃"
 * weather: "晴"
 * weatherCode: "00"
 * weatherPic: "d00"
 * windDir: "西北风"
 * windDirectionDegree: "315"
 * windPower: "2级"
 */
@Data
public class Forecast24h implements Serializable {
    private static final long serialVersionUID = 1L;
    private String forecasttime;
    private String temperature;
    private String weather;
    private String weatherCode;
    private String weatherPic;
    private String windDir;
    private String windDirectionDegree;
    private String windPower;
}
