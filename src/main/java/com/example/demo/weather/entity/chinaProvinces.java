package com.example.demo.weather.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 15:25
 */
@Data
@NoArgsConstructor
@TableName("china_provinces")
public class chinaProvinces implements Serializable {
    private static final long serialVersionUID = 1L;
    private String number;
    private String province;
    //简称 鄂代表湖北
    private String short_name;
}
