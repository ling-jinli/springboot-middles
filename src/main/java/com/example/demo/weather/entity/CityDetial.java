package com.example.demo.weather.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 15:28
 */
@Data
@NoArgsConstructor
@TableName("city_detial")
public class CityDetial implements Serializable {
    private static final long serialVersionUID = 1L;
    // 区
    private String districtcn;
    // 市
    private String namecn;
    // 省份
    private String provcn;
    // cityid
    private String id;
    // 纬度
    private double lat;
    // 经度
    private double lng;
}
