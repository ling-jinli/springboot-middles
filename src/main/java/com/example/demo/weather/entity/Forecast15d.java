package com.example.demo.weather.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 16:00
 * <p>
 * forecasttime: "01/11"
 * reporttime: "2022-01-12 11:00:00.0"
 * sunrise_sunset: "07:21|17:40"
 * temperature_am: "9"
 * temperature_pm: "-1"
 * weather_am: "晴"
 * weather_am_pic: "d00"
 * weather_index_am: "00"
 * weather_index_pm: "00"
 * weather_pm: "晴"
 * weather_pm_pic: "n00"
 * week: "周二"
 * winddir_am: "东南风"
 * winddir_pm: "东南风"
 * windpower_am: "4级"
 * windpower_pm: "4级"
 */
@Data
public class Forecast15d implements Serializable {
    private static final long serialVersionUID = 1L;
    private String forecasttime;
    private String reporttime;
    private String sunrise_sunset;
    private double temperature_am;
    private double temperature_pm;
    private String weather_am;
    private String weather_am_pic;
    private String weather_index_am;
    private String weather_index_pm;
    private String weather_pm;
    private String weather_pm_pic;
    private String week;
    private String winddir_am;
    private String winddir_pm;
    private String windpower_am;
    private String windpower_pm;

}
