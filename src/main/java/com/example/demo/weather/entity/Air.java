package com.example.demo.weather.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 15:46
 * <p>
 * AQI: "156"
 * levelIndex: "中度污染"
 * </p>
 */
@Data
public class Air implements Serializable {
    private static final long serialVersionUID = 1L;
    // 空气质量指数
    private Integer AQI;
    // 污染程度
    private String levelIndex;
}
