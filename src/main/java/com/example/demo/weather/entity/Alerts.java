package com.example.demo.weather.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 15:48
 * <p>
 * alertPic: "icon_warning_1202.png"
 * alertid: "42010041600000_20220111201702_1012001011205"
 * city: "武汉"
 * content: "武汉市气象台2022年01月11日20时17分发布大雾黄色预警信号：预计今天夜间到明天上午，我市将有能见度小于500米，局部小于200米的雾，请注意防范。（预警信息来源：国家预警信息发布中心）"
 * level: "黄色"
 * levelNumber: "02"
 * provice: "湖北"
 * publishTime: "2022-01-11 20:17:02"
 * type: "大雾"
 * typeNumber: "12"
 * </p>
 */
@Data
public class Alerts implements Serializable {
    private static final long serialVersionUID = 1L;
    private String alertPic;
    private String alertid;
    private String city;
    private String content;
    private String level;
    private String levelNumber;
    private String provice;
    private String publishTime;
    private String typeNumber;
}
