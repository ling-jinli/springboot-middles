package com.example.demo.weather.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 原始数据
 *
 * @author lxl & 2022/1/18 10:55 AM
 */
@Data
public class Refer {

    @ApiModelProperty("原始数据来源，或数据源说明，可能为空")
    private List<String> sources;

    @ApiModelProperty("数据许可或版权声明，可能为空")
    private List<String> license;

}
