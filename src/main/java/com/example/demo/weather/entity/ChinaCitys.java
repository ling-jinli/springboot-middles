package com.example.demo.weather.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 15:22
 * [中国城市信息] 省市区县
 */
@Data
@NoArgsConstructor
@TableName("china_citys")
public class ChinaCitys implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cityId;
    private String district;
    private String city;
    private String province;
    private String town;
}
