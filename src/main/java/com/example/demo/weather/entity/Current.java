package com.example.demo.weather.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/1/12 15:52
 * <p>
 * airpressure: "1019"
 * feelstemperature: "6.1"
 * humidity: "69"
 * reporttime: "15:36"
 * temperature: "8"
 * visibility: "2.6"
 * weather: "霾"
 * weatherIndex: "53"
 * weatherPic: "d53"
 * winddir: "西北风"
 * windpower: "2级"
 */
@Data
public class Current implements Serializable {
    private static final long serialVersionUID = 1L;
    private BigDecimal airpressure;
    private BigDecimal feelstemperature;
    private BigDecimal humidity;
    private String reporttime;
    private BigDecimal temperature;
    private BigDecimal visibility;
    private String weather;
    private BigDecimal weatherIndex;
    private String weatherPic;
    private String winddir;
    private String windpower;
}
