package com.example.demo;

import com.example.demo.middle.netty.NettyServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetSocketAddress;

/**
 * @author 子凌
 */
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        //启动服务端
        NettyServer nettyServer = new NettyServer();
        // 监听客户端服务器和客户端端口
        nettyServer.start(new InetSocketAddress("127.0.0.1", 8090));
    }


}
