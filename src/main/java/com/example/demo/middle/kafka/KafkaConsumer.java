package com.example.demo.middle.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 16:12
 * 消息消费
 */
@Slf4j
@Component
public class KafkaConsumer {

    /**
     * @param record record
     * @KafkaListener(groupId = "testGroup", topicPartitions = {
     * @TopicPartition(topic = "topic1", partitions = {"0", "1"}),
     * @TopicPartition(topic = "topic2", partitions = "0",
     * partitionOffsets = @PartitionOffset(partition = "1", initialOffset = "100"))
     * },concurrency = "6")
     * //concurrency就是同组下的消费者个数，就是并发消费数，必须小于等于分区总数
     */
//    @KafkaListener(topics = ParameterConstant.TOPIC_NAME, groupId = ParameterConstant.GROUP1)
    public void listen(ConsumerRecord<?, ?> record) {
        log.info("主题1:{} 内容1：{}", record.topic(), record.value());
    }

    //    @KafkaListener(topics = ParameterConstant.TOPIC_NAME, groupId = ParameterConstant.GROUP2)
    public void listen2(ConsumerRecord<?, ?> record) {
        log.info("主题2:{} 内容2：{}", record.topic(), record.value());
    }
}
