package com.example.demo.middle.kafka;

import com.example.demo.constant.ParameterConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 15:48
 * 消息生产
 */
@Slf4j
@Component
public class KafkaProducer {

    @Autowired
    private KafkaTemplate kafkaTemplate;


    public void send(String msg) {
        log.info("准备发送消息为：{}", msg);
        try {
            //发送消息
            kafkaTemplate.send(ParameterConstant.TOPIC_NAME, ParameterConstant.TOPIC_KEY, "value:" + msg);
            log.info("消息发送成功：{}", msg);
        } catch (Exception e) {
            log.info("消息发送失败：{}", e.getMessage());
        }
    }

}
