package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: ziling
 * @date: created in 下午6:45 2022/7/10
 * @Description:
 */
@Data
@Document(collection = "USER_INFO")
public class UserInfoMongo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String _id;
    private String address;// 用电地址 ADDRESS 用电地址 varchar(64) 64 FALSE FALSE FALSE
    private BigDecimal allCapacity;// 合同容量 ALL_CAPACITY 合同容量 decimal(10,2) 10 2 FALSE FALSE FALSE
    private String contactInformation;// 联系方式 CONTACT_INFORMATION 联系方式 varchar(64) 64 FALSE FALSE FALSE
    private String userNo;// 用户编号 USER_NO 用户编号 varchar(16) 16 TRUE FALSE TRUE
    private String customerNo;// 客户号 CUSTOMER_NO 客户号 varchar(16) 16 FALSE FALSE FALSE
    private String userName;// 用户名称 USER_NAME 用户名称 varchar(64) 64 FALSE FALSE FALSE
    private String deptNo;// 营业区域 DEPT_NO 营业区域 varchar(16) 16 FALSE FALSE FALSE
    private String writeSectionNo;// 抄表区段 WRITE_SECTION_NO 抄表区段 varchar(8) 8 FALSE FALSE FALSE
    private String loadType;// 负荷类别 LOAD_TYPE 负荷类别 varchar(6) 6 FALSE FALSE FALSE
    private String isHighType;// 高可靠性标志 IS_HIGH_TYPE 高可靠性标志 varchar(6) 6 FALSE FALSE FALSE
    private String tempType;// 临时用电标志 TEMP_TYPE 临时用电标志 varchar(6) 6 FALSE FALSE FALSE
    private String creditRankType;// 信誉等级 CREDIT_RANK_TYPE 信誉等级 varchar(6) 6 FALSE FALSE FALSE
    private String writeType;// 抄表周期 WRITE_TYPE 抄表周期 varchar(6) 6 FALSE FALSE FALSE
    private String userType;// 分类标识 USER_TYPE 分类标识Z总户;T结算户;G购电总户 varchar(6) 6 FALSE FALSE FALSE
    private String elecType;// 用电类别 ELEC_TYPE 用电类别 varchar(6) 6 FALSE FALSE FALSE
    private String voltLevelType;// 电压等级 VOLT_LEVEL_TYPE 电压等级 varchar(6) 6 FALSE FALSE FALSE
    private String transformerNo;// 所属台区 TRANSFORMER_NO 所属台区 varbinary(20) 20 FALSE FALSE FALSE
    private String guid;// 原系统用户ID GUID 原系统用户ID varchar(16) 16 FALSE FALSE FALSE
    private String status;// 客户状态 STATUS 客户状态 varchar(6) 6 FALSE FALSE FALSE
    private String userAttach;// 用户归属 USER_ATTACH 用户归属 varchar(64) 64 FALSE FALSE FALSE
    private String linkMan;// 联系人 LINK_MAN 联系人 varchar(64) 64 FALSE FALSE FALSE
    private String spareLinkMan;// 备用联系人 SPARE_LINK_MAN 备用联系人 varchar(64) 64 FALSE FALSE FALSE
    private String spareContactInformation;// 备用联系方式 SPARE_CONTACT_INFORMATION 备用联系方式 varchar(64) 64 FALSE FALSE FALSE
    private String isDoublePower;// 是否申请双电源 IS_DOUBLE_POWER 是否申请双电源 varchar(6) 6 FALSE FALSE FALSE
    private String isOwnTrans;// 是否自备发电机 IS_OWN_TRANS 是否自备发电机 varchar(6) 6 FALSE FALSE FALSE
    private String isHomeSecurity;// 有无家电安保措施 IS_HOME_SECURITY 有无家电安保措施 varchar(6) 6 FALSE FALSE FALSE
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String createDate;// 创建日期 CREATE_DATE 创建日期 datetime FALSE FALSE FALSE
    private Integer modifyPerson;// 操作人 MODIFY_PERSON 数据最近一次操作人 int FALSE FALSE FALSE
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String modifyDate;// 操作时间 MODIFY_DATE 数据最近一次变更时间 datetime FALSE FALSE FALSE
    private String remark;// 备注 REMARK 备注 varchar(256) 256 FALSE FALSE FALSE
    private Integer sn;// 算费次数
    private Integer mon;
    private String userStatus;// 异常状态
    private String meterExceptionType;//表码异常类型
    private String meterExceptionDetail;//表码异常详情
}
