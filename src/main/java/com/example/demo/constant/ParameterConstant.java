package com.example.demo.constant;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2021/12/28 14:26
 * 常量、参数配置中心
 */
public interface ParameterConstant {

    String TOPIC_NAME = "demo";
    String TOPIC_KEY = "ziLing";
    String GROUP1 = "ziLing1";
    String GROUP2 = "ziLing2";

    String WEATHER_CURR_AND_15D_24H_URL = "https://www.weatherol.cn/api/home/getCurrAnd15dAnd24h";
    String WEATHER_RELATED_URL = "https://www.weatherol.cn/api/home/getRelatedWeather";

}
