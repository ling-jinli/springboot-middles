CREATE TABLE `china_citys` (
  `city_id` varchar(100) NOT NULL COMMENT '城市id',
  `district` varchar(100) NOT NULL COMMENT '区',
  `city` varchar(100) NOT NULL COMMENT '市',
  `province` varchar(100) NOT NULL COMMENT '省份/直辖市/港澳台',
  `town` varchar(2000) DEFAULT NULL COMMENT '县/城镇/其他'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='中国省市区县'
