package com.example.communication;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;


@SpringBootTest
public class JwdHttpTest {
    private static final String httpUrl = "https://www.hanting.info/v3/assistant/inputtips?key=2ffb3ff5ce678491637542768b5f8597&keywords=湖北武汉高新6路佛祖岭D区";


    public static void main(String[] args) {
        try {
            URL url = new URL(httpUrl);
            InputStream in = url.openStream();
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader bufr = new BufferedReader(isr);
            String json;
            while ((json = bufr.readLine()) != null) {
                // 将string转成json
                JSONObject json_test = JSONObject.parseObject(json);
                // 将json获取转成数组
                JSONArray array = (JSONArray) json_test.get("tips");
                // 从数组里面匹配对象转换
                Map<String,String> objMap = (Map<String, String>) array.get(0);

                System.out.println(objMap.get("location").split(",")[0]);
                System.out.println(objMap.get("location").split(",")[1]);
            }
            bufr.close();
            isr.close();
            in.close();
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
