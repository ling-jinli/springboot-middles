package com.example.communication.eneity;

import lombok.Data;

import java.io.Serializable;

@Data
public class OauthUser implements Serializable {
    private static final long serialVersionUID = 6962231854266486410L;
    private String userName;
    private Integer registerType;

}
