package com.example.communication.eneity;

import lombok.Data;

import java.io.Serializable;

@Data
public class JsonProperties implements Serializable {
    private static final long serialVersionUID = 6962231854266486410L;
    private String access_token;
    private String refresh_token;
    private OauthUser oauthUser;
    private Long expires_in;
    private String userId;




}
