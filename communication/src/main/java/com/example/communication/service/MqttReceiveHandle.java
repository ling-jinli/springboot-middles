package com.example.communication.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/4/15 17:51
 * @desc [mqtt客户端消息处理类]
 */
@Slf4j
@Component
public class MqttReceiveHandle {
    public void handle(Message<?> message) {
        log.info("主题：{}，QOS:{}，消息接收到的数据：{}", message.getHeaders().get(MqttHeaders.RECEIVED_TOPIC), message.getHeaders().get(MqttHeaders.RECEIVED_QOS), message.getPayload());
    }
}
