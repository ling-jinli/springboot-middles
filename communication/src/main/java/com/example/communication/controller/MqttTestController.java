package com.example.communication.controller;

import com.example.communication.service.MqttGateway;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/4/15 17:54
 * @desc [xxx]
 */
@RestController
@RequestMapping("/test")
public class MqttTestController {
    @Resource
    private MqttGateway mqttGateway;

    /**
     * sendData 消息
     * topic 订阅主题
     **/
    @RequestMapping("/sendMqtt1")
    public String sendMqtt(String sendData, String topic) {
        mqttGateway.sendToMqtt(topic, sendData);
        return "OK";
    }

    /**
     * sendData 消息
     * qos 消息级别 （对应QOS0、QOS1，QOS2）
     * topic 订阅主题
     **/
    @RequestMapping("/sendMqtt2")
    public String sendMqtt(String sendData, int qos, String topic) {
        mqttGateway.sendToMqtt(topic, qos, sendData);
        return "OK";
    }

//    public static void main(String[] args) throws Exception {
//        // 日期格式化
//        DateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date startDate = simpleFormat.parse("2021-10-24");
//        Date endDate = simpleFormat.parse("2022-11-22");
//        long startTime = startDate.getTime();
//        long endTime = endDate.getTime();
//        int days = (int) ((endTime - startTime) / (1000 * 60 * 60 * 24));
//        System.out.println("我和欢宝一共在一起：【" + days+"】天");
//    }

//    public static void main(String[] args) {
//        LocalDate startDate = LocalDate.of(2021, 10, 24);
//        LocalDate endDate = LocalDate.of(2022, 11, 22);
//        long days = ChronoUnit.YEARS.between(startDate, endDate);
//        System.out.println("我和欢宝一共在一起：【" + days+"】天");
//    }
}
