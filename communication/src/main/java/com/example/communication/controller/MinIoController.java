package com.example.communication.controller;

import com.example.communication.minio.MinIoUploadResDTO;
import com.example.communication.minio.MinIoUtils;
import com.example.communication.minio.Result;
import io.minio.errors.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/6/1 9:39
 * @desc [xxx]
 */
@RestController
@RequestMapping("/minio")
public class MinIoController {
    @Resource
    private MinIoUtils minIoUtils;

    // 存储桶名称
    private static final String MINIO_BUCKET = "ngy";

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Result upload(@RequestParam(value = "files") MultipartFile[] files) {
        List<MinIoUploadResDTO> resDTOS = new ArrayList<>();
        //判断file数组不能为空并且长度大于0
        if (files != null && files.length > 0) {
            //循环获取file数组中得文件
            for (int i = 0; i < files.length; i++) {
                MultipartFile file = files[i];
                try {
                    UUID uuid = UUID.randomUUID();
                    String randomId = uuid.toString().replace("-", "");
                    System.out.println("随机生产编码：" + randomId);

                    MinIoUploadResDTO upload = minIoUtils.upload(file, MINIO_BUCKET, randomId);
                    System.out.println(Result.ok(upload));
                    resDTOS.add(upload);
                } catch (Exception e) {
                    return Result.error(e.getMessage());
                }
            }
        }
        return Result.ok(resDTOS.toString());
    }

    @GetMapping("/download")
    public void download(@RequestParam("minFileName") String minFileName, HttpServletResponse response) {
        minIoUtils.download(response, MINIO_BUCKET, minFileName);
    }

    @GetMapping("/download-appoint")
    public void downloadAppoint(@RequestParam("minFileName") String minFileName, HttpServletResponse response) {
        minIoUtils.downloadAppointDirectory(response, MINIO_BUCKET, minFileName);
    }

    @GetMapping("/copy-file")
    public void copyFile(@RequestParam("oldmMinFileName") String oldmMinFileName, @RequestParam("newMinFileName") String newMinFileName, HttpServletResponse response) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        minIoUtils.copyFile(response, MINIO_BUCKET, oldmMinFileName, newMinFileName);
    }
}
