package com.example.communication.minio;

import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/6/1 9:25
 * @desc [xxx]
 */
@Data
public class MinIoUploadResDTO implements Serializable {
    private static final long serialVersionUID = 475040120689218785L;
    private String minFileName;
    private String minFileUrl;

    public MinIoUploadResDTO(String minFileName, String minFileUrl) {
        this.minFileName = minFileName;
        this.minFileUrl = minFileUrl;
    }
}
