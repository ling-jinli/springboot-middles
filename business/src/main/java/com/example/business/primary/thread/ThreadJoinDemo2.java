package com.example.business.primary.thread;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/4/19 14:25
 * @desc [实现多线程按着指定顺序执行]
 * 1:在主线程中通过join()方法指定顺序
 * 通过join()方法使当前线程“阻塞”，等待指定线程执行完毕后继续执行。
 */
public class ThreadJoinDemo2 {
    public static void main(String[] args) throws InterruptedException {
        final Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("打开冰箱！");
            }
        });

        final Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("拿出一瓶牛奶！");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        final Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("关上冰箱！");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread1.start();

        thread1.join();
        thread2.start();

        thread2.join();
        thread3.start();

    }
}
