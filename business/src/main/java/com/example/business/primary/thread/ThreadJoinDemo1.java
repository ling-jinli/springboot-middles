package com.example.business.primary.thread;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/4/19 14:19
 * @desc [实现多线程按着指定顺序执行]
 * 1:在子线程中通过join()方法指定顺序
 * 通过join()方法使当前线程“阻塞”，等待指定线程执行完毕后继续执行。
 */
public class ThreadJoinDemo1 {
    public static void main(String[] args) throws InterruptedException {
        final Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("打开冰箱！");
            }
        });

        final Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//                    等线程1执行完了再来执行当前的线程
                    thread1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("拿出一瓶牛奶！");
            }
        });

        final Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    thread2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("关上冰箱！");
            }
        });

        //下面三行代码顺序可随意调整，程序运行结果不受影响，因为我们在子线程中通过“join()方法”已经指定了运行顺序。
        thread3.start();
        thread2.start();
        thread1.start();
    }
}
