package com.example.business.primary.thread;

import java.util.concurrent.CountDownLatch;

/**
 * @version 1.0
 * @author: ling jinli
 * @date: 2022/4/19 14:48
 * @desc [xxx]
 * 1 概念
 * CountDownLatch接收一个int型参数，表示要等待的工作线程的个数。
 * 当然也不一定是多线程，在单线程中可以用这个int型参数表示多个操作步骤。
 * 2 方法
 * CountDownLatch 提供了一些方法：
 * 方法	说明
 * await()	使当前线程进入同步队列进行等待，直到latch的值被减到0或者当前线程被中断，当前线程就会被唤醒。
 * await(long timeout, TimeUnit unit)	带超时时间的await()。
 * countDown()	使latch的值减1，如果减到了0，则会唤醒所有等待在这个latch上的线程。
 * getCount()	获得latch的数值。
 */
public class ThreadCountDownLatchDemo {

    private static CountDownLatch countDownLatch1 = new CountDownLatch(1);
    private static CountDownLatch countDownLatch2 = new CountDownLatch(1);

    public static void main(String[] args) {
        final Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("打开冰箱！");
                countDownLatch1.countDown();
            }
        });

        final Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    countDownLatch1.await();
                    System.out.println("拿出一瓶牛奶！");
                    countDownLatch2.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        final Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    countDownLatch2.await();
                    System.out.println("关上冰箱！");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        //下面三行代码顺序可随意调整，程序运行结果不受影响
        thread2.start();
        thread3.start();
        thread1.start();
    }
}
